const express = require('express')
const app = express()
const port = 8080

app.set('views', './views')
app.set('view engine', 'pug')
app.use(express.static('public'))

// Route at Root '/' that returns a Test Text message
app.get('/', (req, res) => res.send('Hello World again!!!'))
// Route at '/hello' that returns a Test View
app.get('/hello', function (_req, res) 
{
    res.render('index', { title: 'Hello World', message: 'Hello world!' })
})

// Start the Server
app.listen(port, () => console.log(`Example app listening on port ${port}!`)) 
